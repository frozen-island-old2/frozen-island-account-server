declare module "api-token" {
    export function setExpirationTime(duration: number): void;
    export function isTokenValid(token: string): boolean;
    export function addUser(username: string): user;
    export function findUserByToken(token: string): user;
}

declare interface apiToken {
    setExpirationTime(duration: number): void;
    isTokenValid(token: string): boolean;
    addUser(username: string): user;
    findUserByToken(token: string): user;
}

declare interface user {
    token: string;
    username: string;
}
