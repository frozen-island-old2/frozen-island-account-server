import AccountDatabase from 'account-database-interface';

import Datastore from 'nedb-promises';
import bcrypt from 'bcrypt';
const baseUser: user = {
  username: "String",
  banned: false,
  verified: false,
  admin: false,
  special: false,
  items: [],
};
const baseUnsafeUser: unsafeUser = {
    ...baseUser,
    password: "bcrypyted Password"
}

const saltRounds = 10;

interface databaseResult {
    success: boolean,
    error?: Error
}

interface user {
    username: string,
    banned: boolean,
    verified: boolean,
    admin: boolean,
    special: boolean,
    items: any[],
}

interface unsafeUser extends user{
    password: string;
}

export default class implements AccountDatabase{
    private userDB: Datastore;
    constructor() {
        this.userDB = new Datastore({filename: './.data/users.nedb', autoload: true});
    }

    async addUser(username: string, password: string): Promise<databaseResult> {
        password = await bcrypt.hash(password, saltRounds);
        let newUser = {...baseUnsafeUser, username, password};
        try{
            await this.userDB.insert(newUser);
        } catch (error){
            return {success: false, error};
        }
        return {success:true};
    }

    async getUser(username: string): Promise<unsafeUser|false>{
        let doc = await this.userDB.findOne({username});
        if(doc === null){
            return false;
        } else {
            let user = {...baseUnsafeUser, ...doc};
            return user;
        }
    }

    // This ensures that the password can never hit the outside world.
    async getUserSafe(username: string): Promise<user|false> {
        let userDoc = await this.getUser(username);
        if(userDoc == false){
            return false;
        } else {

            return {...baseUser, ...userDoc};
        }
    }

    async authUser(username: string, password: string): Promise<boolean>{
        let userDoc = await this.getUser(username);
        if(userDoc == false){
            return false;
        } else {
            if(this.isEnabled(userDoc)){
                return false;
            } else {
                return await bcrypt.compare(password, userDoc.password);
            }
        }
    }

    isAdmin(user:user): boolean{
        return user.admin;
    }

    isEnabled(user:user): boolean{
        return (user.verified && !(user.banned || user.special));
    }

    async verifyUser(username: string): Promise<databaseResult> {
        try{
            await this.userDB.update({username}, {$set:{verified: true}}, {});
            return {success: true};
        } catch (error){
            return {success:false, error};
        }
    }
}
