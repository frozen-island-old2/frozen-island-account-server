var OFF = 0, WARN = 1, ERROR = 2;

module.exports = {
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 9,
        "sourceType": "script"
    },
    "rules": {
        "no-bitwise": WARN,
        "no-case-declarations": OFF,
    },
    "env": {
        "node": true
    },
    "globals": {
        "Promise": "readonly"
    }
};
