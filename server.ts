// server.js
// where your node app starts

// init project
import express from 'express';
import bodyParser from 'body-parser';
var app = express();

// parse application/json
app.use(bodyParser.json())

import AccountSystem from './accountSystem/accountSystem';
var accountSystem = new AccountSystem();

import Filter from "example-filter";
var filter = new Filter();

import API1 from "frozen-island-api-v1-server";
var api1 = new API1(app, accountSystem, filter.filter);


// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (_request, response) {
  response.sendFile(__dirname + '/views/index.html');
});

api1.addOwnEndpoints();

// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
    if(listener == null || listener.address == null || typeof listener.address !== "function"){
        console.error("What da!");
        process.exit();
    } else {
        let address = listener.address();
        if(address !== null && typeof address !== "string"){
            console.log('Your app is listening on ' + address.port);
        } else {
            console.error("What da 2!");
            process.exit();
        }
    }
});
